import {Component, inject} from '@angular/core';
import {HotelFormComponent} from "../hotel-form/hotel-form.component";
import {HotelService} from "../../../services/hotel.service";
import {Hotel} from "../../../domain/hotel";

@Component({
  selector: 'app-add-hotel',
  standalone: true,
  imports: [
    HotelFormComponent
  ],
  templateUrl: './hotel-add.component.html',
  styleUrl: './hotel-add.component.css'
})
export class HotelAddComponent {

  hotelService = inject(HotelService);

  hotel: Hotel = {
    address: "",
    city: {id: 0, name: ''},
    description: "",
    distanceToCenter: 0,
    id: 0,
    image: "",
    name: "",
    stars: 0
  };

  addHotel(hotel: Hotel) {
    return this.hotelService.insertHotel(hotel);
  }

}
