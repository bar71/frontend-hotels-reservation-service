import {Component, inject} from '@angular/core';
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {CityService} from "../../../services/city.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-city',
  standalone: true,
  imports: [
    InputTextModule,
    ButtonModule,
    CardModule,
    ReactiveFormsModule
  ],
  templateUrl: './city-add.component.html',
  styleUrl: './city-add.component.css'
})
export class CityAddComponent {
  fb = inject(FormBuilder);

  cityService = inject(CityService);

  router = inject(Router);

  cityForm = this.fb.group({
    name: ["", [Validators.required]]
  });

  get name() {
    return this.cityForm.controls.name;
  }

  addCity() {
    const name = this.name.value!;
    this.cityService.insertCity(name).subscribe({
      next: val => {
        this.router.navigate(["/maintainer/cities"]);
      }
    });
  }

}
