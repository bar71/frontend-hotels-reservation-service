import {Component, inject, OnInit} from '@angular/core';
import {RoomFormComponent} from "../room-form/room-form.component";
import {RoomService} from "../../../services/room.service";
import {Room} from "../../../domain/room";
import {Hotel} from "../../../domain/hotel";
import {ActivatedRoute} from "@angular/router";
import {NgIf} from "@angular/common";
import {CardModule} from "primeng/card";

@Component({
  selector: 'app-room-edit',
  standalone: true,
    imports: [
        RoomFormComponent,
        NgIf,
        CardModule
    ],
  templateUrl: './room-edit.component.html',
  styleUrl: './room-edit.component.css'
})
export class RoomEditComponent implements OnInit {

  roomService = inject(RoomService);

  dataLoaded = false;

  room!: Room;

  route = inject(ActivatedRoute);

  ngOnInit(): void {
    const roomId = this.route.snapshot.params['roomId'];
    this.roomService.selectRoomById(roomId).subscribe({
      next: value => {
        this.room = value;
        this.dataLoaded = true;
      }
    });
  }

  editRoom(room: Room) {
    return this.roomService.updateRoom(room);
  }

}
