import {Component, inject} from '@angular/core';
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {TableModule} from "primeng/table";
import {HotelService} from "../../../services/hotel.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Room} from "../../../domain/room";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {NgIf} from "@angular/common";
import {MessageService} from "primeng/api";
import {TooltipModule} from "primeng/tooltip";
import {RoomService} from "../../../services/room.service";

@Component({
  selector: 'app-hotel-rooms',
  standalone: true,
    imports: [
        ButtonModule,
        ToastModule,
        TableModule,
        ProgressSpinnerModule,
        NgIf,
        TooltipModule
    ],
  providers: [MessageService],
  templateUrl: './rooms.component.html',
  styleUrl: './rooms.component.css'
})
export class RoomsComponent {

  hotelId!: number;

  roomClassId!: number;

  roomId!: number;

  placesCount!: number;

  roomService = inject(RoomService);

  rooms: Room[] = [];

  dataLoaded = false;

  route = inject(ActivatedRoute);

  router = inject(Router);

  constructor() {
  }

  ngOnInit(): void {
    this.hotelId = this.route.snapshot.params['hotelId'];
    this.route.queryParams.subscribe(params => {
      this.roomClassId = params["roomClassId"];
      this.roomId = params["roomId"];
      this.placesCount = params["placesCount"];
    });
    this.roomService.selectRooms(
      {
        placesCount: this.placesCount,
        roomClassId: this.roomClassId,
        id: this.roomId,
        hotelId: this.hotelId
      }).subscribe({
      next: value => {
        this.rooms = value;
        this.dataLoaded = true;
      }
    });
  }

  editRoom(roomId: number) {
    this.router.navigate(["edit", roomId], {relativeTo: this.route});
  }

  deleteRoom(roomId: number) {
    this.router.navigate(["edit", roomId], {relativeTo: this.route});
  }

}
