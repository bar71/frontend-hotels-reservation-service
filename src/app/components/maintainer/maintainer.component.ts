import { Component } from '@angular/core';
import {MenubarModule} from "primeng/menubar";
import {MenuItem} from "primeng/api";

@Component({
  selector: 'app-maintainer',
  standalone: true,
  imports: [
    MenubarModule
  ],
  templateUrl: './maintainer.component.html',
  styleUrl: './maintainer.component.css'
})
export class MaintainerComponent {
  menuItems: MenuItem[] =[
    {
      label: 'Пользователи',
      icon: 'pi pi-users',
      routerLink: ["users"]
    },
    {
      label: 'Отели',
      icon: 'pi pi-home',
      items: [
        {
          label: 'Добавить отель',
          icon: 'pi pi-fw pi-plus',
          routerLink: ["hotels", "add"]
        },
        {
          label: 'Вывести список',
          icon: 'pi pi-fw pi-home',
          routerLink: ["hotels"]
        }
      ]
    },
    {
      label: 'Города',
      icon: '',
      items: [
        {
          label: 'Добавить город',
          icon: 'pi pi-fw pi-plus',
          routerLink: ["cities", "add"]
        },
        {
          label: 'Вывести список',
          icon: '',
          routerLink: ["cities"]
        }
      ]
    },
    {
      label: 'Чёрный список',
      icon: 'pi pi-fw pi-ban',
      items: [
        {
          label: 'Добавить пользователя',
          icon: 'pi pi-fw pi-plus',
          routerLink: ["app-bans", "add"]
        },
        {
          label: 'Вывести список',
          icon: 'pi pi-fw pi-ban',
          routerLink: ["app-bans"]
        }
      ]
    }
  ]
}
