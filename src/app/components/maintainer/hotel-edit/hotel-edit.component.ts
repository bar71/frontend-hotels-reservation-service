import {Component, inject, OnInit} from '@angular/core';
import {HotelFormComponent} from "../hotel-form/hotel-form.component";
import {HotelService} from "../../../services/hotel.service";
import {Hotel} from "../../../domain/hotel";
import {ActivatedRoute} from "@angular/router";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-edit-hotel',
  standalone: true,
  imports: [
    HotelFormComponent,
    NgIf
  ],
  templateUrl: './hotel-edit.component.html',
  styleUrl: './hotel-edit.component.css'
})
export class HotelEditComponent implements OnInit {
  hotelService = inject(HotelService);

  dataLoaded = false;

  hotel!: Hotel;

  route = inject(ActivatedRoute);

  ngOnInit(): void {
    let hotelId = this.route.snapshot.params['hotelId'];
    this.hotelService.selectHotelById(hotelId).subscribe({
      next: value => {
        this.hotel = value;
        this.dataLoaded = true;
      }
    });
  }

  editHotel(hotel: Hotel) {
    return this.hotelService.updateHotel(hotel);
  }

}
