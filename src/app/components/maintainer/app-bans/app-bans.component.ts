import {Component, inject} from '@angular/core';
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {ToastModule} from "primeng/toast";
import {AppBanService} from "../../../services/app-ban.service";
import {ButtonModule} from "primeng/button";
import {AppBan} from "../../../domain/ban/app-ban";

@Component({
  selector: 'app-app-bans',
  standalone: true,
  imports: [
    NgIf,
    ProgressSpinnerModule,
    SharedModule,
    TableModule,
    ToastModule,
    ButtonModule
  ],
  providers: [MessageService],
  templateUrl: './app-bans.component.html',
  styleUrl: './app-bans.component.css'
})
export class AppBansComponent {

  messageService = inject(MessageService);

  dataLoaded: boolean = false;

  appBanService = inject(AppBanService);

  blackList!: AppBan[];

  ngOnInit(): void {
    this.appBanService.selectAppBans().subscribe({
      next: value => {
        this.blackList = value;
        this.dataLoaded = true;
      },
      error: err => {
        console.log(err);
      }
    });
  }

  unBan(userId: number) {
    this.appBanService.delete(userId).subscribe({
      next: value => {
        this.blackList.splice(this.blackList.findIndex(value => userId === value.user.id), 1);
        this.messageService.add({severity: "success", summary: "Успех!", detail: 'Пользователь успешно разблокирован' });
      },
      error: err => {
        console.log(err);
        this.messageService.add({severity: "danger", summary: "Произошла ошибка!"});
      }
    });
  }

}
