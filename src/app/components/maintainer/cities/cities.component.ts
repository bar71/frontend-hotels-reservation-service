import {Component, inject} from '@angular/core';
import {ButtonModule} from "primeng/button";
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {RatingModule} from "primeng/rating";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {ToastModule} from "primeng/toast";
import {CityService} from "../../../services/city.service";
import {City} from "../../../domain/city";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cities',
  standalone: true,
  imports: [
    ButtonModule,
    NgIf,
    ProgressSpinnerModule,
    RatingModule,
    SharedModule,
    TableModule,
    ToastModule
  ],
  providers: [MessageService],
  templateUrl: './cities.component.html',
  styleUrl: './cities.component.css'
})
export class CitiesComponent {

  router = inject(Router);

  cityService = inject(CityService);

  cities: City[] = [];

  dataLoaded = false;

  constructor() {
  }

  ngOnInit(): void {
    this.cityService.selectAllCities().subscribe({
      next: value => {
        this.cities = value;
        this.dataLoaded = true;
      }
    })
  }

  showCityHotels(cityId: number) {
    this.router.navigate(["/maintainer/hotels"], {
      queryParams: {
        cityId: cityId,
      }
    });
  }

  editCity(cityId: number) {
    this.router.navigate(["/maintainer/cities/edit", cityId]);
  }

}
