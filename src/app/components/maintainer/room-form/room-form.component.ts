import {Component, inject, Input, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {Room} from "../../../domain/room";
import {RoomClass} from "../../../domain/room-class";
import {RoomClassService} from "../../../services/room-class.service";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {Dropdown, DropdownModule} from "primeng/dropdown";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {RatingModule} from "primeng/rating";
import {TooltipModule} from "primeng/tooltip";

@Component({
  selector: 'app-room-form',
  standalone: true,
  imports: [
    ButtonModule,
    CardModule,
    DropdownModule,
    InputTextModule,
    NgIf,
    ProgressSpinnerModule,
    RatingModule,
    ReactiveFormsModule,
    TooltipModule
  ],
  templateUrl: './room-form.component.html',
  styleUrl: './room-form.component.css'
})
export class RoomFormComponent {

  @Input({required: true}) room!: Room;

  @Input({required: true}) submitFn!: (Room: Room) => Observable<Room>;

  router = inject(Router);

  fb = inject(FormBuilder);

  roomClassService = inject(RoomClassService);

  roomClasses: RoomClass[] = [];

  dataLoaded = false;

  activatedRoot = inject(ActivatedRoute);

  roomForm = this.fb.group({
    roomClass: [{id: 0, name: ''}, [Validators.required]],
    costPerDay: [0, [Validators.required]],
    description: ['', [Validators.required]],
    placesCount: [0, [Validators.required]],
  });

  get roomClass() {
    return this.roomForm.controls.roomClass;
  }

  get description() {
    return this.roomForm.controls.description;
  }

  get costPerDay() {
    return this.roomForm.controls.costPerDay;
  }

  get placesCount() {
    return this.roomForm.controls.placesCount;
  }

  ngOnInit(): void {
    this.roomClassService.selectAllRoomClasses().subscribe({
      next: value => {
        this.roomClasses = value;
        this.room.hotelId = this.activatedRoot.snapshot.params["hotelId"];
        this.dataLoaded = true;
        setTimeout(() => {
          this.updateControls();
        });
      }
    });
  }

  updateControls() {
    this.placesCount.setValue(this.room.placesCount);
    this.description.setValue(this.room.description);
    this.costPerDay.setValue(this.room.costPerDay);
    this.roomClass.setValue(this.room.roomClass);
    if (this.room.roomClass.id === 0) {
      this.roomClass.reset();
    } else {
      this.roomClass.setValue(this.room.roomClass);
    }
  }

  submit() {
    Object.assign(this.room, this.roomForm.value);
    this.submitFn(this.room).subscribe({
      next: value => {
        this.router.navigate(["/maintainer/hotels", this.room.hotelId, "rooms"]);
      }
    })
  }
}
