import {Component, inject} from '@angular/core';
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {PaginatorModule} from "primeng/paginator";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, Validators, ReactiveFormsModule} from "@angular/forms";
import {AppBanRequestDto} from "../../../domain/ban/app-ban-request-dto";
import {HotelAdminService} from "../../../services/hotel-admin.service";
import {HotelAdmin} from "../../../domain/hotel-admin";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-hotel-admin-add',
  standalone: true,
  imports: [
    ButtonModule,
    CardModule,
    InputTextModule,
    NgIf,
    PaginatorModule,
    ReactiveFormsModule
  ],
  templateUrl: './hotel-admin-add.component.html',
  styleUrl: './hotel-admin-add.component.css'
})
export class HotelAdminAddComponent {

  hotelAdminService = inject(HotelAdminService);

  userService = inject(UserService);

  router = inject(Router);

  route = inject(ActivatedRoute);

  fb = inject(FormBuilder);


  hotelAdminForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
  });

  get email() {
    return this.hotelAdminForm.controls.email;
  }

  addHotelAdmin() {
    this.userService.selectUsers({email: this.email.value!}).subscribe({
      next: value => {
        if (value.length != 0) {
          this.addAdmin(value[0].id);
        }
      }
    })
  }

  addAdmin(userId: number) {
    const hotelId: number = this.route.snapshot.params["hotelId"];
    let hotelAdmin: HotelAdmin = {
      userId: userId,
      hotelId: hotelId
    };
    this.hotelAdminService.insertHotelAdmin(hotelAdmin).subscribe({
      next: value => {
        this.router.navigate(["maintainer", "users"], {
          queryParams: {
            hotelId: hotelId,
          }
        });
      }
    });
  }


}
