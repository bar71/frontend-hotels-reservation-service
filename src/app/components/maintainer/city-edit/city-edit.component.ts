import {Component, inject, OnInit} from '@angular/core';
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {CityService} from "../../../services/city.service";
import {ActivatedRoute, Router} from "@angular/router";
import {City} from "../../../domain/city";

@Component({
  selector: 'app-edit-city',
  standalone: true,
    imports: [
        ButtonModule,
        CardModule,
        InputTextModule,
        ReactiveFormsModule
    ],
  templateUrl: './city-edit.component.html',
  styleUrl: './city-edit.component.css'
})
export class CityEditComponent implements OnInit {

  fb = inject(FormBuilder);

  cityService = inject(CityService);

  router = inject(Router);

  route = inject(ActivatedRoute);

  city!: City;

  cityForm = this.fb.group({
    name: ["", [Validators.required]]
  });

  ngOnInit(): void {
    let cityId = this.route.snapshot.params['cityId'];
    this.cityService.selectCityById(cityId).subscribe({
      next: value => {
        this.city = value;
        this.name.setValue(this.city.name);
      }
    })
  }

  get name() {
    return this.cityForm.controls.name;
  }

  editCity() {
    const name = this.name.value!;
    this.city.name = name;
    this.cityService.updateCity(this.city).subscribe({
      next: value => {
        this.router.navigate(["/maintainer/cities"]);
      }
    });
  }
}
