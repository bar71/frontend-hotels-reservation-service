import {Component, inject} from '@angular/core';
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {AppBanService} from "../../../services/app-ban.service";
import {Router} from "@angular/router";
import {AppBanRequestDto} from "../../../domain/ban/app-ban-request-dto";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {DropdownModule} from "primeng/dropdown";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {RatingModule} from "primeng/rating";
import {TooltipModule} from "primeng/tooltip";

@Component({
  selector: 'app-app-bans-add',
  standalone: true,
  imports: [
    ButtonModule,
    CardModule,
    DropdownModule,
    InputTextModule,
    NgIf,
    RatingModule,
    ReactiveFormsModule,
    TooltipModule
  ],
  templateUrl: './app-bans-add.component.html',
  styleUrl: './app-bans-add.component.css'
})
export class AppBansAddComponent {

  appBanService = inject(AppBanService);

  router = inject(Router);

  fb = inject(FormBuilder);

  appBanForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    reason: ['', [Validators.required]]
  });

  get email() {
    return this.appBanForm.controls.email;
  }

  get reason() {
    return this.appBanForm.controls.reason;
  }

  addAppBan() {
    let appBanRequestDto: AppBanRequestDto = {
      email: "",
      reason: ""
    };
    Object.assign(appBanRequestDto, this.appBanForm.value);
    this.appBanService.insert(appBanRequestDto).subscribe({
      next: value => {
        this.router.navigate(["maintainer", "app-bans"])
      }
    })
  }

}
