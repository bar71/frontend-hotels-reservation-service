import {Component, inject} from '@angular/core';
import {HotelService} from "../../../services/hotel.service";
import {Hotel} from "../../../domain/hotel";
import {ButtonModule} from "primeng/button";
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {MenuItem, MenuItemCommandEvent, MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {RatingModule} from "primeng/rating";
import {ToastModule} from "primeng/toast";
import {FormsModule} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {MenuModule} from "primeng/menu";

@Component({
  selector: 'app-hotels',
  standalone: true,
  imports: [
    ButtonModule,
    NgIf,
    ProgressSpinnerModule,
    SharedModule,
    TableModule,
    RatingModule,
    ToastModule,
    FormsModule,
    MenuModule
  ],
  providers: [MessageService],
  templateUrl: './hotels.component.html',
  styleUrl: './hotels.component.css'
})
export class HotelsComponent {

  cityId!: number;

  stars!: number;

  hotelService = inject(HotelService);

  hotels: Hotel[] = [];

  dataLoaded = false;

  route = inject(ActivatedRoute);

  router = inject(Router);

  constructor() {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.cityId = params['cityId'];
      this.stars = params['stars'];
    });

    this.hotelService.selectHotels({cityId: this.cityId, stars: this.stars}).subscribe({
      next: value => {
        this.hotels = value;
        this.dataLoaded = true;
      }
    });
  }

  addRoom(hotelId: number) {
    this.router.navigate([hotelId, "rooms", "add"], {relativeTo: this.route});
  }

  editHotel(hotelId: number) {
    this.router.navigate(["edit", hotelId], {relativeTo: this.route});
  }

  showHotelRooms(hotelId: number) {
    this.router.navigate([hotelId, "rooms"], {relativeTo: this.route});
  }

  showHotelBans(hotelId: number) {
    this.router.navigate([hotelId, "bans"], {relativeTo: this.route});
  }

  showHotelAdmins(hotelId: number) {
    this.router.navigate(["/maintainer", "users"], {
      queryParams: {hotelId: hotelId}
    });
  }

  addHotelAdmin(hotelId: number) {
    this.router.navigate(["/maintainer", "hotels", hotelId, "admins", "add"]);
  }
}
