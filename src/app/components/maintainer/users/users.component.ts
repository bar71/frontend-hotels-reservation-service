import {Component, inject, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {User} from "../../../domain/user";
import {JsonPipe, NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ButtonModule} from "primeng/button";
import {MessageService, SharedModule} from "primeng/api";
import {TableModule} from "primeng/table";
import {ToastModule} from "primeng/toast";
import {ActivatedRoute} from "@angular/router";
import {HotelAdminService} from "../../../services/hotel-admin.service";

@Component({
  selector: 'app-users',
  standalone: true,
  imports: [
    NgIf,
    ProgressSpinnerModule,
    ButtonModule,
    SharedModule,
    TableModule,
    ToastModule,
    JsonPipe
  ],
  providers: [MessageService],
  templateUrl: './users.component.html',
  styleUrl: './users.component.css'
})
export class UsersComponent implements OnInit {

  userService = inject(UserService);

  hotelAdminService = inject(HotelAdminService);

  users: User[] = [];

  dataLoaded = false;

  route = inject(ActivatedRoute);

  constructor() {
  }

  ngOnInit(): void {
    let hotelId: number | undefined;
    let email: string | undefined;
    this.route.queryParams.subscribe(params => {
      hotelId = params['hotelId'];
      email = params['email'];
    });
    this.userService.selectUsers({hotelId: hotelId, email: email}).subscribe({
      next: value => {
        this.users = value;
        this.dataLoaded = true;
      }
    })
  }

  unAdminUser(userId: number, index: number) {
    this.hotelAdminService.deleteHotelAdmin(userId).subscribe({
      next: value => {
        this.users[index].roles.splice(
          this.users[index].roles.findIndex(value => value.name === "ADMIN"), 1
        );
      }
    });
  }
}
