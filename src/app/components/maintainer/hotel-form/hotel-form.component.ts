import {
  AfterViewInit,
  Component,
  inject,
  Input,
  OnChanges,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {Hotel} from "../../../domain/hotel";
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {Router} from "@angular/router";
import {CityService} from "../../../services/city.service";
import {City} from "../../../domain/city";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {RatingModule} from "primeng/rating";
import {Dropdown, DropdownItem, DropdownModule} from "primeng/dropdown";
import {TooltipModule} from "primeng/tooltip";
import {DataViewModule} from "primeng/dataview";
import {ProgressSpinnerModule} from "primeng/progressspinner";

@Component({
  selector: 'app-hotel-form',
  standalone: true,
  imports: [
    ButtonModule,
    CardModule,
    InputTextModule,
    ReactiveFormsModule,
    NgIf,
    RatingModule,
    DropdownModule,
    TooltipModule,
    DataViewModule,
    ProgressSpinnerModule
  ],
  templateUrl: './hotel-form.component.html',
  styleUrl: './hotel-form.component.css'
})
export class HotelFormComponent implements OnInit {

  @Input({required: true}) hotel!: Hotel;

  @Input({required: true}) submitFn!: (hotel: Hotel) => Observable<Hotel>;

  router = inject(Router);

  fb = inject(FormBuilder);

  cityService = inject(CityService);

  dataLoaded = false;

  cities: City[] = [];

  hotelForm = this.fb.group({
    name: ["", [Validators.required]],
    city: [{id: 0, name: ''}, [Validators.required]],
    image: ["", [Validators.required]],
    stars: [0, [Validators.required]],
    address: ["", [Validators.required]],
    description: ["", [Validators.required]],
    distanceToCenter: [0, [Validators.required]]
  });

  ngOnInit(): void {
    this.cityService.selectAllCities().subscribe({
      next: value => {
        this.cities = value;
        this.dataLoaded = true;
        setTimeout(() => {
          this.updateControls();
        });
      }
    });
  }

  updateControls() {
    this.name.setValue(this.hotel.name);
    if (this.hotel.city.id === 0) {
      this.city.reset();
    } else {
      this.city.setValue(this.hotel.city);
    }
    this.image.setValue(this.hotel.image);
    this.stars.setValue(this.hotel.stars);
    this.address.setValue(this.hotel.address);
    this.description.setValue(this.hotel.description);
    this.distanceToCenter.setValue(this.hotel.distanceToCenter);
  }

  submit() {
    Object.assign(this.hotel, this.hotelForm.value);
    this.submitFn(this.hotel).subscribe({
      next: value => {
        this.router.navigate(["/maintainer/hotels"]);
      }
    })
  }


  get name() {
    return this.hotelForm.controls.name;
  }

  get city() {
    return this.hotelForm.controls.city;
  }

  get image() {
    return this.hotelForm.controls.image;
  }

  get stars() {
    return this.hotelForm.controls.stars;
  }

  get address() {
    return this.hotelForm.controls.address;
  }

  get description() {
    return this.hotelForm.controls.description;
  }

  get distanceToCenter() {
    return this.hotelForm.controls.distanceToCenter;
  }

  uploadImage(files: FileList | null) {
    if (files) {
      let reader = new FileReader()
      reader.readAsDataURL(files[0]);
      reader.onload = (event:Event) => {
        let fileReader = event.target as FileReader;
        this.image.setValue(fileReader.result as string);
      }
    }
  }
}

