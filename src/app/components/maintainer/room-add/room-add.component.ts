import {Component, inject} from '@angular/core';
import {RoomService} from "../../../services/room.service";
import {Room} from "../../../domain/room";
import {HotelFormComponent} from "../hotel-form/hotel-form.component";
import {RoomFormComponent} from "../room-form/room-form.component";

@Component({
  selector: 'app-room-add',
  standalone: true,
  imports: [
    HotelFormComponent,
    RoomFormComponent
  ],
  templateUrl: './room-add.component.html',
  styleUrl: './room-add.component.css'
})
export class RoomAddComponent {

  roomService = inject(RoomService);

  room: Room = {
    costPerDay: 0,
    description: "",
    hotelId: 0,
    id: 0,
    placesCount: 0,
    roomClass: {id: 0, name: ""}
  };

  addRoom(room: Room) {
    return this.roomService.insertRoom(room);
  }
}
