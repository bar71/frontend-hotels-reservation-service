import {Component, inject} from '@angular/core';
import {HotelBanService} from "../../../services/hotel-ban.service";
import {MessageService, SharedModule} from "primeng/api";
import {HotelBan} from "../../../domain/ban/hotel-ban";
import {Hotel} from "../../../domain/hotel";
import {ActivatedRoute} from "@angular/router";
import {ButtonModule} from "primeng/button";
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {TableModule} from "primeng/table";
import {ToastModule} from "primeng/toast";

@Component({
  selector: 'app-hotel-bans',
  standalone: true,
  imports: [
    ButtonModule,
    NgIf,
    ProgressSpinnerModule,
    SharedModule,
    TableModule,
    ToastModule
  ],
  providers: [MessageService],
  templateUrl: './hotel-bans.component.html',
  styleUrl: './hotel-bans.component.css'
})
export class HotelBansComponent {
  dataLoaded: boolean = false;

  hotelBanService = inject(HotelBanService);

  blackList!: HotelBan[];

  route = inject(ActivatedRoute);

  hotelId!: number;

  ngOnInit(): void {
    this.hotelId = this.route.snapshot.params["hotelId"];
    this.hotelBanService.selectHotelBansByHotelId(this.hotelId).subscribe({
      next: value => {
        this.blackList = value;
        this.dataLoaded = true;
      },
      error: err => {
        console.log(err);
      }
    });
  }

}
