import {Component, inject} from '@angular/core';
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {PaginatorModule} from "primeng/paginator";
import {TabViewModule} from "primeng/tabview";
import {AuthService} from "../../services/auth.service";
import {User} from "../../domain/user";
import {Router, RouterLink} from "@angular/router";
import {SignUpRequest} from "../../domain/auth/sign-up-request";
import {passwordValidator} from "../../validators/password.validator";
import {HeaderComponent} from "../header/header.component";
import {MenuItemsService} from "../../services/menu-items.service";

@Component({
  selector: 'app-sign-up',
  standalone: true,
    imports: [
        ButtonModule,
        CardModule,
        InputTextModule,
        NgIf,
        PaginatorModule,
        ReactiveFormsModule,
        TabViewModule,
        RouterLink
    ],
  templateUrl: './sign-up.component.html',
  styleUrl: './sign-up.component.css',
})
export class SignUpComponent {

  menuItemsService = inject(MenuItemsService);

  authService = inject(AuthService);

  formBuilder = inject(FormBuilder);

  router = inject(Router);



  signUpForm = this.formBuilder.group({
    name: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    rawPassword: ['', [Validators.required]],
    confirmPassword: ['', [Validators.required]],
  }, {
    validators: passwordValidator
  });

  get name() {
    return this.signUpForm.controls.name;
  }

  get email() {
    return this.signUpForm.controls.email;
  }

  get phone() {
    return this.signUpForm.controls.phone;
  }

  get rawPassword() {
    return this.signUpForm.controls.rawPassword;
  }

  get confirmPassword() {
    return this.signUpForm.controls.confirmPassword;
  }

  signUp() {
    const signUpRequest = this.signUpForm.value as SignUpRequest;

    this.authService.signUp(signUpRequest).subscribe({
      next: value => {
        localStorage.setItem("user", JSON.stringify(value.user));
        localStorage.setItem("jwt", value.jwt);
        this.newMenu();
        this.router.navigate(["/"]);
      },
      error: err => console.log(err)
    })
  }

  newMenu() {
    this.menuItemsService.changeMenu("")
  }

}
