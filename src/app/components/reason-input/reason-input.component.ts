import {Component, inject} from '@angular/core';
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {DynamicDialogRef} from "primeng/dynamicdialog";

@Component({
  selector: 'app-reason-input',
  standalone: true,
  imports: [
    InputTextModule,
    ButtonModule
  ],
  templateUrl: './reason-input.component.html',
  styleUrl: './reason-input.component.css'
})
export class ReasonInputComponent {

  ref = inject(DynamicDialogRef);

  onSubmit(reason: string) {
    this.ref.close(reason);
  }

}
