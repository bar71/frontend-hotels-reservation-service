import {Component, inject, OnInit} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgForOf, NgIf} from "@angular/common";
import {ReservationComponent} from "../reservation/reservation.component";
import {SharedModule} from "primeng/api";
import {UserService} from "../../../services/user.service";
import {User} from "../../../domain/user";
import {Reservation} from "../../../domain/reservation";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ReservationService} from "../../../services/reservation.service";

@Component({
  selector: 'app-paid',
  standalone: true,
    imports: [
        DataViewModule,
        NgForOf,
        ReservationComponent,
        SharedModule,
        NgIf,
        ProgressSpinnerModule
    ],
  templateUrl: './paid.component.html',
  styleUrl: './paid.component.css'
})
export class PaidComponent implements OnInit{

  reservationService = inject(ReservationService);

  reservations: Reservation[] = [];

  status: string = "Оплачено";

  severity: string = "success";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectUserPaidReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

}
