import { Component } from '@angular/core';
import {User} from "../../../domain/user";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {RouterLink} from "@angular/router";
import {SharedModule} from "primeng/api";

@Component({
  selector: 'app-info',
  standalone: true,
  imports: [
    ButtonModule,
    CardModule,
    FormsModule,
    InputTextModule,
    NgIf,
    ReactiveFormsModule,
    RouterLink,
    SharedModule
  ],
  templateUrl: './info.component.html',
  styleUrl: './info.component.css'
})
export class InfoComponent {
  user: User = JSON.parse(localStorage.getItem("user")!) as User;
}
