import {Component, inject} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgForOf, NgIf} from "@angular/common";
import {ReservationComponent} from "../reservation/reservation.component";
import {MessageService, SharedModule} from "primeng/api";
import {UserService} from "../../../services/user.service";
import {User} from "../../../domain/user";
import {Reservation} from "../../../domain/reservation";
import {ReservationService} from "../../../services/reservation.service";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {Router} from "@angular/router";
import {DividerModule} from "primeng/divider";
import {ProgressSpinnerModule} from "primeng/progressspinner";

@Component({
  selector: 'app-waiting-payment',
  standalone: true,
  imports: [
    DataViewModule,
    NgForOf,
    ReservationComponent,
    SharedModule,
    ButtonModule,
    ToastModule,
    DividerModule,
    NgIf,
    ProgressSpinnerModule
  ],
  providers: [MessageService],
  templateUrl: './waiting-payment.component.html',
  styleUrl: './waiting-payment.component.css'
})
export class WaitingPaymentComponent {

  reservationService = inject(ReservationService);

  messageService = inject(MessageService);

  reservations: Reservation[] = [];

  status: string = "Ожидает оплаты";

  severity: string = "primary";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectUserWaitingPaymentReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

  pay(reservationId: number) {
    this.reservationService.pay(reservationId).subscribe({
      next: value => {
        this.messageService.add({ severity: 'success', summary: 'Успех!', detail: 'Ваша резервация успешно оплачена' });
        this.reservations.splice(this.reservations.
          findIndex((value) => {return value.id === reservationId}),
          1
        );
      },
      error: err => console.log(err)
    });
  }

}
