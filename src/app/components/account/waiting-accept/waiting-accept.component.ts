import {Component, inject, OnInit} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {ReservationComponent} from "../reservation/reservation.component";
import {UserService} from "../../../services/user.service";
import {User} from "../../../domain/user";
import {Reservation} from "../../../domain/reservation";
import {NgFor, NgIf} from "@angular/common";
import {ButtonModule} from "primeng/button";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ReservationService} from "../../../services/reservation.service";

@Component({
  selector: 'app-waiting-accept',
  standalone: true,
    imports: [
        DataViewModule,
        ReservationComponent,
        NgFor,
        ButtonModule,
        NgIf,
        ProgressSpinnerModule
    ],
  templateUrl: './waiting-accept.component.html',
  styleUrl: './waiting-accept.component.css'
})
export class WaitingAcceptComponent implements OnInit {

  reservationService = inject(ReservationService);

  reservations: Reservation[] = [];

  status: string = "Ожидает подтверждения";

  severity: string = "warning";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectUserWaitingAcceptReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

}
