import {Component, Input} from '@angular/core';
import {Reservation} from "../../../domain/reservation";
import {ButtonModule} from "primeng/button";
import {JsonPipe, NgClass, NgForOf} from "@angular/common";
import {SharedModule} from "primeng/api";
import {TagModule} from "primeng/tag";

@Component({
  selector: 'app-reservation',
  standalone: true,
  imports: [
    ButtonModule,
    NgForOf,
    SharedModule,
    TagModule,
    NgClass,
    JsonPipe
  ],
  templateUrl: './reservation.component.html',
  styleUrl: './reservation.component.css'
})
export class ReservationComponent {

  @Input({required: true}) reservation!: Reservation;

  @Input({required: true}) severity!: string;

  @Input({required: true}) status!: string;

  protected readonly String = String;

  get startDate() {
    return String(this.reservation.startDate).split("T")[0];
  }

  get finalDate() {
    return String(this.reservation.finalDate).split("T")[0];
  }

}
