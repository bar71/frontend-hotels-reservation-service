import {Component, inject} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {MenuItem} from "primeng/api";
import {MenuModule} from "primeng/menu";
import {UserService} from "../../services/user.service";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-profile',
  standalone: true,
  imports: [
    RouterOutlet,
    MenuModule,
    NgIf
  ],
  templateUrl: './account.component.html',
  styleUrl: './account.component.css'
})
export class AccountComponent {

  userService = inject(UserService);

  isAdmin = this.userService.sessionUserIsAdmin();

  clientItems: MenuItem[] =[
    {
      label: "Панель гостя",
      items: [
        {
          label: "Аккаунт",
          icon: "pi pi-id-card",
          routerLink: "info",
        },
        {
          label: "Ожидают подтверждения",
          icon: "",
          routerLink: "waiting-accept"
        },
        {
          label: "Ожидают оплаты",
          icon: "",
          routerLink: "waiting-payment"
        },
        {
          label: "Оплаченные",
          icon: "",
          routerLink: "paid"
        }
      ]
    }
  ];

}
