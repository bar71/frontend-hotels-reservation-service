import {Component, inject, OnInit} from '@angular/core';
import {SearchFormComponent} from "../search-form/search-form.component";
import {CityService} from "../../services/city.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Hotel} from "../../domain/hotel";
import {NgClass, NgFor, NgIf} from "@angular/common";
import {DataViewModule} from "primeng/dataview";
import {RatingModule} from "primeng/rating";
import {TagModule} from "primeng/tag";
import {FormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {DropdownModule} from "primeng/dropdown";
import {SelectItem} from "primeng/api";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {HotelService} from "../../services/hotel.service";

@Component({
  selector: 'app-search',
  standalone: true,
    imports: [
        SearchFormComponent,
        NgFor,
        DataViewModule,
        RatingModule,
        TagModule,
        FormsModule,
        NgClass,
        ButtonModule,
        DropdownModule,
        NgIf,
        ProgressSpinnerModule
    ],
  templateUrl: './search-hotels.component.html',
  styleUrl: './search-hotels.component.css'
})
export class SearchHotelsComponent implements OnInit {

  sortOptions!: SelectItem[];

  sortOrder!: number;
  sortKey!: string;
  sortField!: string;

  hotelService = inject(HotelService);
  router = inject(Router);
  route = inject(ActivatedRoute);
  cityName = "";
  startDate = "";
  finalDate = "";
  roomClassId = 0;
  placesCount = 0;

  hotels: Hotel[] = [];

  dataLoaded = false;

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.cityName = params['cityName'];
      this.startDate = params['startDate'];
      this.finalDate = params['finalDate'];
      this.roomClassId = params['roomClassId'];
      this.placesCount = params['placesCount'];
    });

    this.hotelService.selectFilteredHotels({
      cityName: this.cityName,
      startDate: this.startDate,
      finalDate: this.finalDate,
      roomClassId: this.roomClassId,
      placesCount: this.placesCount}).subscribe({
      next: value => {
        this.hotels = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });

    this.sortOptions = [
      { label: 'Дальше от центра города', value: '!distanceToCenter' },
      { label: 'Ближе к центру города', value: 'distanceToCenter' }
    ];

  }

  onSortChange(event: any) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  showRooms(id: number) {
    this.router.navigate(["/choose-room"], {
      queryParams: {
        hotelId: id,
        startDate: this.startDate,
        finalDate: this.finalDate,
        roomClassId: this.roomClassId,
        placesCount: this.placesCount
      }
    })
  }

}
