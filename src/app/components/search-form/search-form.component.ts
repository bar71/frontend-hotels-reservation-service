import {Component, inject, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators, ReactiveFormsModule} from "@angular/forms";
import {ButtonModule} from "primeng/button";
import {Router} from "@angular/router";
import {RoomClass} from "../../domain/room-class";
import {InputTextModule} from "primeng/inputtext";
import {CascadeSelectModule} from "primeng/cascadeselect";
import {RoomClassService} from "../../services/room-class.service";
import {NgFor, NgIf} from "@angular/common";
import {TableModule} from "primeng/table";
import {Dropdown, DropdownModule} from "primeng/dropdown";
import {CityService} from "../../services/city.service";
import {CalendarModule} from "primeng/calendar";
import {InputGroupModule} from "primeng/inputgroup";
import {InputGroupAddonModule} from "primeng/inputgroupaddon";
import {AutoCompleteCompleteEvent, AutoCompleteModule} from "primeng/autocomplete";
import {searchDateRangeValidator} from "../../validators/searchDateRange.validator";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {TooltipModule} from "primeng/tooltip";
import {City} from "../../domain/city";

@Component({
  selector: 'app-search-form',
  standalone: true,
  imports: [ReactiveFormsModule, ButtonModule, InputTextModule, CascadeSelectModule, NgIf, NgFor, TableModule, DropdownModule, CalendarModule, InputGroupModule, InputGroupAddonModule, AutoCompleteModule, ProgressSpinnerModule, TooltipModule],
  templateUrl: './search-form.component.html',
  styleUrl: './search-form.component.css'
})
export class SearchFormComponent implements OnInit{

  formBuilder = inject(FormBuilder);

  roomClassService = inject(RoomClassService);

  cityService = inject(CityService);

  router = inject(Router);

  roomClasses: RoomClass[] = [];

  dataLoaded = false;

  cities: City[] = [];

  filteredCities: City[] = [];

  searchForm = this.formBuilder.group({
      city: [{id: 0, name: ''}, [Validators.required]],
      startDate: [new Date(Date.now() + 24 * 3600 * 1000), [Validators.required]],
      finalDate: [new Date(Date.now() + 24 * 3600 * 1000 * 2), [Validators.required]],
      roomClass: [{id: 0, name: ''}, [Validators.required]],
      placesCount: [1, [Validators.required]]
    }, {
      validators: searchDateRangeValidator
    }
  );

  constructor() {

  }

  ngOnInit(): void {
    this.cityService.selectAllCities().subscribe({
      next: value => {
        this.cities = value;
      },
      error: err => console.log(err)
    });
    this.roomClassService.selectAllRoomClasses().subscribe({
      next: value => {
        this.roomClasses = value;
        this.dataLoaded = true;
        setTimeout(() => {
          this.updateControls();
        });
      },
      error: err => console.log(err)
    });
  }

  updateControls() {
    if (localStorage.getItem("city"))
      this.city.setValue(JSON.parse(localStorage.getItem("city")!) as City);
    if (this.city.value?.id === 0) {
      this.city.reset();
    }
    if (localStorage.getItem("startDate"))
      this.startDate.setValue(localStorage.getItem("startDate") as unknown as Date);
    if (localStorage.getItem("finalDate"))
      this.finalDate.setValue(localStorage.getItem("finalDate") as unknown as Date);
    if (localStorage.getItem("roomClass"))
      this.roomClass.setValue(JSON.parse(localStorage.getItem("roomClass")!) as RoomClass);
    if (localStorage.getItem("placesCount"))
      this.placesCount.setValue(Number(localStorage.getItem("placesCount")));
    if (this.roomClass.value?.id === 0) {
      this.roomClass.reset();
    }
  }

  filterCity(event: AutoCompleteCompleteEvent) {
    let filtered: City[] = [];
    let query = event.query;
    for (let i = 0; i < this.cities.length; i++) {
      let city = this.cities[i];
      if (city.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(city);
      }
    }
    this.filteredCities = filtered;
  }

  formatDate(date: Date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  submitFn() {
    let cityName!: string;
    if (this.city.value?.name) {
      localStorage.setItem("city", JSON.stringify(this.city.value));
      cityName = this.city.value?.name;
    }
    else {
      cityName = String(this.city.value);
    }
    const startDate = this.formatDate(new Date(this.startDate.value!));
    const finalDate = this.formatDate(new Date(this.finalDate.value!));
    const roomClassId = (this.roomClass.value! as unknown as RoomClass).id;
    const placesCount = this.placesCount.value!;
    localStorage.setItem("startDate", startDate);
    localStorage.setItem("finalDate", finalDate);
    localStorage.setItem("roomClass", JSON.stringify(this.roomClass.value! as unknown as RoomClass));
    localStorage.setItem("placesCount", String(placesCount));

    this.router.navigate(
      ["/search"],
      {
        queryParams: {
          cityName: cityName,
          startDate: startDate,
          roomClassId: roomClassId,
          finalDate: finalDate,
          placesCount: placesCount
        }
      });
  }

  get city() {
    return this.searchForm.controls.city;
  }

  get startDate() {
    return this.searchForm.controls.startDate;
  }

  get finalDate() {
    return this.searchForm.controls.finalDate;
  }

  get roomClass() {
    return this.searchForm.controls.roomClass;
  }

  get placesCount() {
    return this.searchForm.controls.placesCount;
  }

  increase() {
    let v = this.placesCount.value!;
    if (v !== 9) {
      v++;
      this.placesCount.setValue(v);
    }
  }

  decrease() {
    let v = this.placesCount.value!;
    if (v !== 1) {
      v--;
      this.placesCount.setValue(v);
    }
  }
}
