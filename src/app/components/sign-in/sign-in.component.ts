import {Component, inject, Injectable} from '@angular/core';
import {FormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {NgIf} from "@angular/common";
import {ButtonModule} from "primeng/button";
import {TabViewModule} from "primeng/tabview";
import {AuthEmailRequest} from "../../domain/auth/auth-email-request";
import {AuthService} from "../../services/auth.service";
import {Router, RouterLink} from "@angular/router";
import {AuthPhoneRequest} from "../../domain/auth/auth-phone-request";
import {AuthResponse} from "../../domain/auth/auth-response";
import {UserService} from "../../services/user.service";
import {HeaderComponent} from "../header/header.component";
import {MenuItemsService} from "../../services/menu-items.service";

@Component({
  selector: 'app-sign-in',
  standalone: true,
  imports: [
    CardModule,
    ReactiveFormsModule,
    InputTextModule,
    NgIf,
    ButtonModule,
    TabViewModule,
    RouterLink
  ],
  templateUrl: './sign-in.component.html',
  styleUrl: './sign-in.component.css',
})
export class SignInComponent {

  menuItemsService = inject(MenuItemsService);

  authService = inject(AuthService);

  userService = inject(UserService);

  router = inject(Router);

  formBuilder = inject(FormBuilder);

  failed: boolean = false;

  failedError: string = "";

  newMenu() {
    this.menuItemsService.changeMenu("");
  }

  signInByEmailForm = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    rawPassword: ['', [Validators.required]]
  });

  get email() {
    return this.signInByEmailForm.controls.email;
  }

  get rawPasswordEmail() {
    return this.signInByEmailForm.controls.rawPassword;
  }

  signInByPhoneForm = this.formBuilder.group({
    phone: ['', [Validators.required]],
    rawPassword: ['', [Validators.required]]
  });

  get phone() {
    return this.signInByPhoneForm.controls.phone;
  }

  get rawPasswordPhone() {
    return this.signInByPhoneForm.controls.rawPassword;
  }

  signInByEmail() {
    const authEmailRequest = this.signInByEmailForm.value as AuthEmailRequest;
    this.authService.signInByEmail(authEmailRequest).subscribe({
      next: value => {
        this.successSignIn(value);
      },
      error: err => {
        console.log(err);
        this.setFailedError("Неверный email или пароль");
      }
    });
  }

  signInByPhone() {
    const authPhoneRequest = this.signInByPhoneForm.value as AuthPhoneRequest;

    this.authService.signInByPhone(authPhoneRequest).subscribe({
      next: value => {
        this.successSignIn(value);
      },
      error: err => {
        console.log(err);
        this.setFailedError("Неверный номер телефона или пароль");
      }
    });
  }

  successSignIn(authResponse: AuthResponse) {
    localStorage.setItem("user", JSON.stringify(authResponse.user));
    localStorage.setItem("jwt", authResponse.jwt);
    this.newMenu();
    this.router.navigate(["/"]);

  }

  setFailedError(failedError: string) {
    this.failedError = failedError;
    this.failed = true;
  }

}
