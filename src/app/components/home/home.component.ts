import {Component, inject} from '@angular/core';
import {SearchFormComponent} from "../search-form/search-form.component";
import {CardModule} from "primeng/card";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-home-search-form',
  standalone: true,
  imports: [SearchFormComponent, CardModule, NgIf],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
    protected readonly localStorage = localStorage;
    protected readonly JSON = JSON;
}
