import {Component, Input} from '@angular/core';
import {User} from "../../../domain/user";

@Component({
  selector: 'app-guest',
  standalone: true,
  imports: [],
  templateUrl: './guest.component.html',
  styleUrl: './guest.component.css'
})
export class GuestComponent {
  @Input({required: true}) guest!: User;
}
