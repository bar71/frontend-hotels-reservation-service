import {Component, inject, OnInit} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgFor, NgIf} from "@angular/common";
import {GuestComponent} from "../guest/guest.component";
import {ButtonModule} from "primeng/button";
import {TableModule} from "primeng/table";
import {MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {HotelBan} from "../../../domain/ban/hotel-ban";
import {HotelBanService} from "../../../services/hotel-ban.service";
import {Hotel} from "../../../domain/hotel";
import {HotelService} from "../../../services/hotel.service";

@Component({
  selector: 'app-black-list',
  standalone: true,
  imports: [
    DataViewModule,
    NgIf,
    GuestComponent,
    NgFor,
    ButtonModule,
    TableModule,
    ToastModule,
    ProgressSpinnerModule
  ],
  providers: [MessageService],
  templateUrl: './black-list.component.html',
  styleUrl: './black-list.component.css'
})
export class BlackListComponent implements OnInit {

  dataLoaded: boolean = false;

  hotelService = inject(HotelService);

  hotelBanService = inject(HotelBanService);

  messageService = inject(MessageService);

  blackList!: HotelBan[];

  ngOnInit(): void {
    this.hotelService.selectHotelBans().subscribe({
      next: value => {
        this.blackList = value;
        this.dataLoaded = true;
      },
      error: err => {
        console.log(err);
      }
    });
  }

  unBan(userId: number, hotelId: number) {
    this.hotelBanService.delete(userId, hotelId).subscribe({
      next: value => {
        this.blackList.splice(this.blackList.findIndex(value => userId === value.user.id), 1);
        this.messageService.add({severity: "success", summary: "Успех!", detail: 'Пользователь успешно разблокирован' });
      },
      error: err => {
        console.log(err);
        this.messageService.add({severity: "danger", summary: "Произошла ошибка!"});
      }
    });
  }

}
