import {Component, inject} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgForOf, NgIf} from "@angular/common";
import {MessageService, SharedModule} from "primeng/api";
import {DividerModule} from "primeng/divider";
import {GuestComponent} from "../guest/guest.component";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ReservationComponent} from "../../account/reservation/reservation.component";
import {HotelService} from "../../../services/hotel.service";
import {Reservation} from "../../../domain/reservation";
import {Hotel} from "../../../domain/hotel";
import {ButtonModule} from "primeng/button";
import {User} from "../../../domain/user";
import {ReasonInputComponent} from "../../reason-input/reason-input.component";
import {DialogService, DynamicDialogRef} from "primeng/dynamicdialog";
import {HotelBanService} from "../../../services/hotel-ban.service";
import {ReservationService} from "../../../services/reservation.service";

@Component({
  selector: 'app-admin-waiting-payment',
  standalone: true,
    imports: [
        DataViewModule,
        NgForOf,
        ReservationComponent,
        SharedModule,
        DividerModule,
        GuestComponent,
        NgIf,
        ProgressSpinnerModule,
        ButtonModule
    ],
  providers: [MessageService, DialogService],
  templateUrl: './admin-waiting-payment.component.html',
  styleUrl: './admin-waiting-payment.component.css'
})
export class AdminWaitingPaymentComponent {

  ref: DynamicDialogRef | undefined;

  dialogService = inject(DialogService);

  hotelBanService = inject(HotelBanService);

  messageService = inject(MessageService);

  reservationService = inject(ReservationService);

  reservations: Reservation[] = [];

  status: string = "Ожидает оплаты";

  severity: string = "primary";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectHotelWaitingPaymentReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

  enterBanReason(userId: number) {
    this.ref = this.dialogService.open(ReasonInputComponent, {header: "Введите причину блокировки"});
    this.ref.onClose.subscribe((reason: string) => {
        if (reason) {
          this.ban(userId, reason);
        }
      }
    )
  }

  ban(userId: number, reason: string) {
    this.hotelBanService.insert({userId: userId, reason: reason}).subscribe({
      next: value => {
        let index = 0;
        while((index = this.reservations.findIndex(value => userId === value.user.id)) != -1) {
          this.reservations.splice(index, 1);
          index = this.reservations.findIndex(value => userId === value.user.id);
        }
        this.messageService.add({severity: "success", summary: "Успех!", detail: 'Пользователь успешно заблокирован' });
      },
      error: err => {
        console.log(err);
        this.messageService.add({severity: "error", summary: "Произошла ошибка!"});
      }
    });
  }

  notSessionUser(userId: number) {
    return userId != (JSON.parse(localStorage.getItem("user")!) as User).id;
  }

  ngOnDestroy(): void {
    this.ref?.close();
  }

}
