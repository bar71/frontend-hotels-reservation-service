import {Component, inject, OnDestroy} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgFor, NgIf} from "@angular/common";
import {ButtonModule} from "primeng/button";
import {ToastModule} from "primeng/toast";
import {MessageService} from "primeng/api";
import {DividerModule} from "primeng/divider";
import {GuestComponent} from "../guest/guest.component";
import {DialogService, DynamicDialogRef} from "primeng/dynamicdialog";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ReservationComponent} from "../../account/reservation/reservation.component";
import {HotelService} from "../../../services/hotel.service";
import {ReservationService} from "../../../services/reservation.service";
import {HotelBanService} from "../../../services/hotel-ban.service";
import {Hotel} from "../../../domain/hotel";
import {Reservation} from "../../../domain/reservation";
import {ReasonInputComponent} from "../../reason-input/reason-input.component";
import {User} from "../../../domain/user";

@Component({
  selector: 'app-admin-waiting-accept',
  standalone: true,
  imports: [
    ReservationComponent,
    DataViewModule,
    NgFor,
    ButtonModule,
    ToastModule,
    DividerModule,
    GuestComponent,
    NgIf,
    ProgressSpinnerModule
  ],
  providers: [MessageService, DialogService],
  templateUrl: './admin-waiting-accept.component.html',
  styleUrl: './admin-waiting-accept.component.css'
})
export class AdminWaitingAcceptComponent implements OnDestroy {

  ref: DynamicDialogRef | undefined;

  dialogService = inject(DialogService);

  reservationService = inject(ReservationService);

  hotelBanService = inject(HotelBanService);

  messageService = inject(MessageService);

  reservations: Reservation[] = [];

  status: string = "Ожидает подтверждения";

  severity: string = "warning";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectHotelWaitingAcceptReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

  accept(reservationId: number) {
    this.reservationService.accept(reservationId).subscribe({
      next: value => {
        this.messageService.add({ severity: 'success', summary: 'Успех!', detail: 'Резервация принята' });
        this.reservations.splice(this.reservations.
          findIndex((value) => {return value.id === reservationId}),
          1
        );
      },
      error: err => console.log(err)
    });
  }

  reject(reservationId: number) {
    this.reservationService.reject(reservationId).subscribe({
      next: value => {
        this.messageService.add({ severity: 'Error', summary: 'Успех!', detail: 'Резервация отклонена' });
        this.reservations.splice(this.reservations.
          findIndex((value) => {return value.id === reservationId}),
          1
        );
      },
      error: err => console.log(err)
    });
  }

  enterBanReason(userId: number) {
    this.ref = this.dialogService.open(ReasonInputComponent, {header: "Введите причину блокировки"});
    this.ref.onClose.subscribe((reason: string) => {
        if (reason) {
          this.ban(userId, reason);
        }
      }
    )
  }

  ban(userId: number, reason: string) {
    this.hotelBanService.insert({userId: userId, reason: reason}).subscribe({
      next: value => {
        let index = 0;
        while((index = this.reservations.findIndex(value => userId === value.user.id)) != -1) {
          this.reservations.splice(index, 1);
          index = this.reservations.findIndex(value => userId === value.user.id);
        }
        this.messageService.add({severity: "success", summary: "Успех!", detail: 'Пользователь успешно заблокирован' });
      },
      error: err => {
        this.messageService.add({severity: "error", summary: "Произошла ошибка!"});
      }
    });
  }

  notSessionUser(userId: number) {
    return userId != (JSON.parse(localStorage.getItem("user")!) as User).id;
  }

  ngOnDestroy(): void {
    this.ref?.close();
  }

}
