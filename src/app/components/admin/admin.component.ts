import {Component, inject} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {MenuItem} from "primeng/api";
import {MenuModule} from "primeng/menu";
import {UserService} from "../../services/user.service";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-admin',
  standalone: true,
  imports: [
    RouterOutlet,
    MenuModule,
    NgIf
  ],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css'
})
export class AdminComponent {

  userService = inject(UserService);

  isAdmin = this.userService.sessionUserIsAdmin();

  adminItems: MenuItem[] =[
    {
      label: "Панель администратора",
      items: [
        {
          label: "Информация об отеле",
          icon: "pi pi-building",
          routerLink: "hotel-info",
        },
        {
          label: "Ожидают подтверждения",
          icon: "",
          routerLink: "waiting-accept"
        },
        {
          label: "Ожидают оплаты",
          icon: "",
          routerLink: "waiting-payment"
        },
        {
          label: "Оплаченные",
          icon: "",
          routerLink: "paid"
        },
        {
          label: "Черный список",
          icon: "pi pi-ban",
          routerLink: "black-list"
        }
      ]
    }
  ];

}
