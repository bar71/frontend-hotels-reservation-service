import {Component, inject} from '@angular/core';
import {DataViewModule} from "primeng/dataview";
import {NgFor, NgIf} from "@angular/common";
import {DividerModule} from "primeng/divider";
import {GuestComponent} from "../guest/guest.component";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {ReservationComponent} from "../../account/reservation/reservation.component";
import {HotelService} from "../../../services/hotel.service";
import {Hotel} from "../../../domain/hotel";
import {Reservation} from "../../../domain/reservation";
import {ReservationService} from "../../../services/reservation.service";

@Component({
  selector: 'app-admin-paid',
  standalone: true,
  imports: [
    DataViewModule,
    ReservationComponent,
    NgFor,
    DividerModule,
    GuestComponent,
    NgIf,
    ProgressSpinnerModule
  ],
  templateUrl: './admin-paid.component.html',
  styleUrl: './admin-paid.component.css'
})
export class AdminPaidComponent {

  reservationService = inject(ReservationService);

  reservations: Reservation[] = [];

  status: string = "Оплачено";

  severity: string = "success";

  dataLoaded = false;

  ngOnInit(): void {
    this.reservationService.selectHotelPaidReservations().subscribe({
      next: value => {
        this.reservations = value;
        this.dataLoaded = true;
      },
      error: err => console.log(err)
    });
  }

}
