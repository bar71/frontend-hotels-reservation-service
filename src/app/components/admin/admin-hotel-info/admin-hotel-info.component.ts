import {Component, inject, OnInit} from '@angular/core';
import {CardModule} from "primeng/card";
import {InputTextModule} from "primeng/inputtext";
import {TagModule} from "primeng/tag";
import {RatingModule} from "primeng/rating";
import {FormsModule} from "@angular/forms";
import {Hotel} from "../../../domain/hotel";
import {HotelService} from "../../../services/hotel.service";
import {NgIf} from "@angular/common";
import {ProgressSpinnerModule} from "primeng/progressspinner";

@Component({
  selector: 'app-admin-hotel-info',
  standalone: true,
  imports: [
    CardModule,
    InputTextModule,
    TagModule,
    RatingModule,
    FormsModule,
    NgIf,
    ProgressSpinnerModule
  ],
  templateUrl: './admin-hotel-info.component.html',
  styleUrl: './admin-hotel-info.component.css'
})
export class AdminHotelInfoComponent implements OnInit{

  hotel!: Hotel;

  hotelService = inject(HotelService);

  dataLoaded = false;

  ngOnInit(): void {
    this.hotelService.selectHotelByAdminId().subscribe({
      next: value => {
        this.hotel = value;
        this.dataLoaded = true;
    }
    })
  }
}
