import {Component, inject} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Hotel} from "../../domain/hotel";
import {HotelService} from "../../services/hotel.service";
import {ButtonModule} from "primeng/button";
import {DataViewModule} from "primeng/dataview";
import {NgClass, NgForOf, NgIf} from "@angular/common";
import {RatingModule} from "primeng/rating";
import {MessageService, SelectItem, SharedModule} from "primeng/api";
import {TagModule} from "primeng/tag";
import {FormsModule} from "@angular/forms";
import {Room} from "../../domain/room";
import {ReservationService} from "../../services/reservation.service";
import {User} from "../../domain/user";
import {ToastModule} from "primeng/toast";
import {DropdownModule} from "primeng/dropdown";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {RoomService} from "../../services/room.service";

@Component({
	selector: 'app-rooms',
	standalone: true,
    imports: [
        ButtonModule,
        DataViewModule,
        NgForOf,
        RatingModule,
        SharedModule,
        TagModule,
        FormsModule,
        NgClass,
        ToastModule,
        DropdownModule,
        NgIf,
        ProgressSpinnerModule
    ],
	templateUrl: './search-rooms.component.html',
	styleUrl: './search-rooms.component.css',
	providers: [MessageService]
})
export class SearchRoomsComponent {

	sortOptions: SelectItem[]	 = [
    { label: 'Дороже', value: '!costPerDay' },
    { label: 'Дешевле', value: 'costPerDay' }
  ];
	sortOrder!: number;
	sortKey!: string;
	sortField!: string;

	reservationService = inject(ReservationService);
	roomService = inject(RoomService);
	messageService = inject(MessageService);

	router = inject(Router);
	route = inject(ActivatedRoute);
	hotelId = 0;
	startDate = "";
	finalDate = "";
	roomClassId = 0;
	placesCount = 0;

	rooms: Room[] = [];

	dataLoaded = false;

	ngOnInit(): void {
		this.route.queryParams.subscribe(params => {
			this.hotelId = params['hotelId'];
			this.startDate = params['startDate'];
			this.finalDate = params['finalDate'];
			this.roomClassId = params['roomClassId'];
			this.placesCount = params['placesCount'];
		});

		this.roomService.selectFilteredRooms({
      hotelId: this.hotelId,
      startDate: this.startDate,
      finalDate: this.finalDate,
      roomClassId: this.roomClassId,
      placesCount: this.placesCount}
    ).subscribe({
			  next: value => {
				this.rooms = value;
				this.dataLoaded = true;
			},
			error: err => console.log(err)
		});
	}

	onSortChange(event: any) {
		let value = event.value;

		if (value.indexOf('!') === 0) {
			this.sortOrder = -1;
			this.sortField = value.substring(1, value.length);
		} else {
			this.sortOrder = 1;
			this.sortField = value;
		}
	}

	reservRoom(roomId: number, costPerDay: number) {
		if (localStorage.getItem("jwt") == null) {
			this.router.navigate(["/sign-in"]);
			return;
		}

		this.reservationService.insertReservation(
      {
        hotelId: this.hotelId,
        roomId: roomId,
        startDate: this.startDate,
        finalDate: this.finalDate,
      }
		).subscribe({
			next: value => {
				this.rooms.splice(this.rooms.findIndex((value) => {
					return value.id === roomId
				}), 1);
				this.messageService.add({ severity: 'success', summary: 'Успех!', detail: 'Забронированные комнаты можно посмотреть в профиле' });
			},
			error: err => {
				console.log(err);
				this.messageService.add({ severity: 'error', summary: 'Ошибка!', detail: 'Не удалось забронировать комнату' });
			}
		});

	}

}
