import {AfterViewInit, Component, inject, Injectable, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ImageModule } from 'primeng/image';
import {Menu, MenuModule} from "primeng/menu";
import {ButtonModule} from "primeng/button";
import {CommonModule, NgIf} from "@angular/common";
import {MenuItem, MenuItemCommandEvent} from "primeng/api";
import {UserService} from "../../services/user.service";
import {MenuItemsService} from "../../services/menu-items.service";
import {accountMenuItem, adminMenuItem, maintainerMenuItem, signOutMenuItem} from "./header-menu-items";


@Component({
  selector: 'app-header',
  standalone: true,
  imports: [ImageModule, MenuModule, ButtonModule, NgIf, CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent implements OnInit, AfterViewInit {

  @ViewChild(Menu)
  private menu!: Menu;

  userService = inject(UserService);

  menuItems: MenuItem[] = [];

  menuItemsService = inject(MenuItemsService);

  protected readonly localStorage = localStorage;

  constructor() {
  }

  ngOnInit(): void {
    this.menuItemsService.currentMenu.subscribe(menu => this.changeMenu());
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.changeMenu();
    });
  }

  changeMenu() {
    this.menuItems = [];
    this.menuItems.push(accountMenuItem);
    if (this.userService.sessionUserIsAdmin()) {
      this.menuItems.push(adminMenuItem);
    }
    if (this.userService.sessionUserIsMaintainer()) {
      this.menuItems.push(maintainerMenuItem);
    }
    this.menuItems.push(signOutMenuItem);
    if (this.menu) {
      // The workaround
      // @ts-ignore
      this.menu.cd.markForCheck();
    }
  }

}
