import {MenuItem, MenuItemCommandEvent} from "primeng/api";

export const accountMenuItem: MenuItem = {
    label: "Аккаунт",
    icon: "pi pi-user",
    routerLink: "/account/info"
};

export const adminMenuItem: MenuItem = {
  label: "Панель администратора",
  icon: "pi pi-cog",
  routerLink: ["/admin/hotel-info"],
}

export const maintainerMenuItem: MenuItem = {
  label: "Управление системой",
  icon: "pi pi-cog",
  routerLink: ["/maintainer"],
}

export const signOutMenuItem: MenuItem = {
  label: "Выйти",
  icon: "pi pi-sign-out",
  routerLink: ["/sign-in"],
  command(event: MenuItemCommandEvent) {
    localStorage.removeItem("jwt");
    localStorage.removeItem("user");
    localStorage.removeItem("hotel");
  }
}
