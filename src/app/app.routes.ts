import { Routes } from '@angular/router';
import {SignInComponent} from "./components/sign-in/sign-in.component";
import {SignUpComponent} from "./components/sign-up/sign-up.component";
import {HomeComponent} from "./components/home/home.component";
import {SearchHotelsComponent} from "./components/search-hotels/search-hotels.component";
import {SearchRoomsComponent} from "./components/search-rooms/search-rooms.component";
import {AccountComponent} from "./components/account/account.component";
import {InfoComponent} from "./components/account/info/info.component";
import {WaitingAcceptComponent} from "./components/account/waiting-accept/waiting-accept.component";
import {WaitingPaymentComponent} from "./components/account/waiting-payment/waiting-payment.component";
import {PaidComponent} from "./components/account/paid/paid.component";
import {adminGuard} from "./guards/admin.guard";
import {authGuard} from "./guards/auth.guard";
import {AdminComponent} from "./components/admin/admin.component";
import {AdminHotelInfoComponent} from "./components/admin/admin-hotel-info/admin-hotel-info.component";
import {AdminWaitingAcceptComponent} from "./components/admin/admin-waiting-accept/admin-waiting-accept.component";
import {AdminWaitingPaymentComponent} from "./components/admin/admin-waiting-payment/admin-waiting-payment.component";
import {AdminPaidComponent} from "./components/admin/admin-paid/admin-paid.component";
import {BlackListComponent} from "./components/admin/black-list/black-list.component";
import {MaintainerComponent} from "./components/maintainer/maintainer.component";
import {maintainerGuard} from "./guards/maintainer.guard";
import {UsersComponent} from "./components/maintainer/users/users.component";
import {HotelsComponent} from "./components/maintainer/hotels/hotels.component";
import {CitiesComponent} from "./components/maintainer/cities/cities.component";
import {RoomsComponent} from "./components/maintainer/rooms/rooms.component";
import {HotelBansComponent} from "./components/maintainer/hotel-bans/hotel-bans.component";
import {AppBansComponent} from "./components/maintainer/app-bans/app-bans.component";
import {CityAddComponent} from "./components/maintainer/city-add/city-add.component";
import {CityEditComponent} from "./components/maintainer/city-edit/city-edit.component";
import {HotelAddComponent} from "./components/maintainer/hotel-add/hotel-add.component";
import {HotelEditComponent} from "./components/maintainer/hotel-edit/hotel-edit.component";
import {RoomAddComponent} from "./components/maintainer/room-add/room-add.component";
import {RoomEditComponent} from "./components/maintainer/room-edit/room-edit.component";
import {AppBansAddComponent} from "./components/maintainer/app-bans-add/app-bans-add.component";
import {HotelAdminAddComponent} from "./components/maintainer/hotel-admin-add/hotel-admin-add.component";

export const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "sign-in", component: SignInComponent},
  {path: "sign-up", component: SignUpComponent},
  {path: "search", component: SearchHotelsComponent},
  {path: "choose-room", component: SearchRoomsComponent},
  {
    path: "account", component: AccountComponent, canActivate: [authGuard], children: [
      {path: "info", component: InfoComponent},
      {path: "waiting-accept", component: WaitingAcceptComponent},
      {path: "waiting-payment", component: WaitingPaymentComponent},
      {path: "paid", component: PaidComponent},
    ]
  },
  {
    path: "admin", component: AdminComponent, canActivate: [authGuard, adminGuard], children: [
      {path: "hotel-info", component: AdminHotelInfoComponent},
      {path: "waiting-accept", component: AdminWaitingAcceptComponent},
      {path: "waiting-payment", component: AdminWaitingPaymentComponent},
      {path: "paid", component: AdminPaidComponent},
      {path: "black-list", component: BlackListComponent}
    ]
  },
  {
    path: "maintainer", component: MaintainerComponent, canActivate: [authGuard, maintainerGuard], children: [
      {path: "users", component: UsersComponent},
      {path: "hotels", component: HotelsComponent},
      {path: "hotels/add", component: HotelAddComponent},
      {path: "hotels/edit/:hotelId", component: HotelEditComponent},
      {path: "hotels/:hotelId/rooms/add", component: RoomAddComponent},
      {path: "hotels/:hotelId/rooms/edit/:roomId", component: RoomEditComponent},
      {path: "hotels/:hotelId/rooms", component: RoomsComponent},
      {path: "hotels/:hotelId/bans", component: HotelBansComponent},
      {path: "hotels/:hotelId/admins/add", component: HotelAdminAddComponent},
      {path: "cities", component: CitiesComponent},
      {path: "cities/edit/:cityId", component: CityEditComponent},
      {path: "cities/add", component: CityAddComponent},
      {path: "app-bans", component: AppBansComponent},
      {path: "app-bans/add", component: AppBansAddComponent}
    ]
  },
  {path: "**", redirectTo: "/"}
];
