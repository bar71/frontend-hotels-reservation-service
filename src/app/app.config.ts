import {ApplicationConfig, importProvidersFrom} from '@angular/core';
import {provideRouter, RouteReuseStrategy} from '@angular/router';

import { routes } from './app.routes';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {MessageService} from "primeng/api";
import {MyRouteStrategyReuse} from "./reuse-route";

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    importProvidersFrom([BrowserAnimationsModule, HttpClientModule]),
    MessageService,
    {provide: RouteReuseStrategy, useClass: MyRouteStrategyReuse}
  ]
};
