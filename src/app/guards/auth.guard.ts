import {CanActivateFn, Router, UrlTree} from '@angular/router';
import {UserService} from "../services/user.service";
import {inject} from "@angular/core";

export const authGuard: CanActivateFn = (route, state): boolean | UrlTree => {
  if (inject(UserService).sessionUserIsLoggedIn()) {
    return true;
  }
  return inject(Router).createUrlTree(["/sign-in"]);
};
