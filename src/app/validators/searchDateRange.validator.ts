import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export const searchDateRangeValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const startDate = control.get('startDate');
  const finalDate = control.get('finalDate');

  if (!startDate || !finalDate) {
    return null;
  }

  const sd = new Date(startDate.value);
  const fd = new Date(finalDate.value);

  const s_sd = `${sd.getFullYear()}-${(sd.getMonth() + 1).toString().padStart(2, '0')}-${sd.getDate().toString().padStart(2, '0')}`
  const s_fd = `${fd.getFullYear()}-${(fd.getMonth() + 1).toString().padStart(2, '0')}-${fd.getDate().toString().padStart(2, '0')}`

  return s_sd < s_fd ? null : {finalDateBigger: true};
}
