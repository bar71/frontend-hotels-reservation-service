import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {HotelBan} from "../domain/ban/hotel-ban";
import {HttpOptionsService} from "./http-options.service";
import {HotelBanRequestDto} from "../domain/ban/hotel-ban-request-dto";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HotelBanService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/hotel-ban`;

  constructor() { }

  selectHotelBansByHotelId(hotelId: number) {
    return this.http.get<HotelBan[]>(`${this.baseUrl}/${hotelId}`, this.httpOptionsService.getHttpOptions());
  }

  insert(hotelBanRequestDto: HotelBanRequestDto) {
    return this.http.post<HotelBan>(`${this.baseUrl}`, hotelBanRequestDto, this.httpOptionsService.getHttpOptions());
  }

  delete(userId: number, hotelId: number) {
    return this.http.delete<HotelBan>(`${this.baseUrl}?userId=${userId}&hotelId=${hotelId}`, this.httpOptionsService.getHttpOptions());
  }

}
