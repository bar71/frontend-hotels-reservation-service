import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RoomClass} from "../domain/room-class";
import {HttpOptionsService} from "./http-options.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RoomClassService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/room-class`;

  constructor() {
  }

  selectAllRoomClasses() {
    return this.http.get<RoomClass[]>(`${this.baseUrl}`, this.httpOptionsService.getHttpOptions());
  }

}
