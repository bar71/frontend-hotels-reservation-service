import {Hotel} from "../domain/hotel";
import {inject, Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Room} from "../domain/room";
import {Reservation} from "../domain/reservation";
import {User} from "../domain/user";
import {HttpOptionsService} from "./http-options.service";
import {HotelBan} from "../domain/ban/hotel-ban";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class HotelService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/hotel`;

  constructor() {
  }

  selectHotels(parameters?: {cityId?: number, stars?: number} ) {
    let params = new HttpParams();
    if (parameters) {
      if (parameters["cityId"]) {
        params = params.append("cityId", parameters["cityId"]);
      }
      if (parameters["stars"]) {
        params = params.append("stars", parameters["stars"]);
      }
    }
    return this.http.get<Hotel[]>(`${this.baseUrl}`, {
      headers: this.httpOptionsService.getHttpHeaders(),
      params: params
    });
  }

  selectHotelById(hotelId: number) {
    return this.http.get<Hotel>(`${this.baseUrl}/${hotelId}`, this.httpOptionsService.getHttpOptions());
  }

  selectHotelByAdminId() {
    return this.http.get<Hotel>(`${this.baseUrl}/by-admin-id`, this.httpOptionsService.getHttpOptions());
  }

  selectFilteredHotels(parameters: {cityName: string,
                       startDate: string,
                       finalDate: string,
                       roomClassId: number,
                       placesCount: number}) {
    let params = new HttpParams().appendAll(parameters);
    return this.http.get<Hotel[]>(`${this.baseUrl}/filtered`,
      {
        params: params,
        headers: this.httpOptionsService.getHttpHeaders()
      });
  }

  selectHotelBans() {
    return this.http.get<HotelBan[]>(`${this.baseUrl}/black-list`, this.httpOptionsService.getHttpOptions());
  }

  insertHotel(hotel: Hotel) {
    return this.http.post<Hotel>(`${this.baseUrl}`, hotel, this.httpOptionsService.getHttpOptions());
  }

  updateHotel(hotel: Hotel) {
    return this.http.put<Hotel>(`${this.baseUrl}`, hotel, this.httpOptionsService.getHttpOptions());
  }

}
