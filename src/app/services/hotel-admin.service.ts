import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Hotel} from "../domain/hotel";
import {City} from "../domain/city";
import {HttpOptionsService} from "./http-options.service";
import {HotelAdmin} from "../domain/hotel-admin";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HotelAdminService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/hotel-admin`;

  constructor() { }

  selectHotelAdmins(parameters?: { userId?: number | undefined, hotelId?: string | undefined }) {
    let params = new HttpParams();
    if (parameters) {
      if (parameters["userId"]) {
        params = params.append("userId", parameters["userId"]);
      }
      if (parameters["hotelId"]) {
        params = params.append("hotelId", parameters["hotelId"]);
      }
    }
    return this.http.get<HotelAdmin[]>(`${this.baseUrl}`,
      { headers: this.httpOptionsService.getHttpHeaders(), params: params } );
  }

  selectHotelAdminsByUserId(userId: number) {
    return this.http.get<HotelAdmin[]>(`${this.baseUrl}/${userId}`, this.httpOptionsService.getHttpOptions());
  }

  insertHotelAdmin(hotelAdmin: HotelAdmin) {
    return this.http.post<HotelAdmin>(`${this.baseUrl}`,
      hotelAdmin,
      this.httpOptionsService.getHttpOptions()
    );
  }

  deleteHotelAdmin(userId: number) {
    return this.http.delete(`${this.baseUrl}/${userId}`,
      this.httpOptionsService.getHttpOptions()
    );
  }

}
