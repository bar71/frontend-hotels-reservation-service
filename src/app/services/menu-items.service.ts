import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MenuItemsService {

  private menuSource = new BehaviorSubject('default message');

  currentMenu = this.menuSource.asObservable();

  constructor() { }

  changeMenu(menu: string) {
    this.menuSource.next(menu)
  }


}
