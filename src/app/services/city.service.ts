import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Hotel} from "../domain/hotel";
import {City} from "../domain/city";
import {HttpOptionsService} from "./http-options.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CityService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/city`;

  constructor() { }

  selectAllCities(parameters?: { id?: number | undefined, name?: string | undefined }) {
    let params = new HttpParams();
    if (parameters) {
      if (parameters["id"]) {
        params = params.append("id", parameters["id"]);
      }
      if (parameters["name"]) {
        params = params.append("name", parameters["name"]);
      }
    }
    return this.http.get<City[]>(`${this.baseUrl}`, {headers: this.httpOptionsService.getHttpHeaders(), params: params} );
  }

  selectCityById(cityId: number) {
    return this.http.get<City>(`${this.baseUrl}/${cityId}`, this.httpOptionsService.getHttpOptions());
  }

  insertCity(name: string) {
    return this.http.post<City>(`${this.baseUrl}`,
      name,
      this.httpOptionsService.getHttpOptions()
    );
  }

  updateCity(city: City) {
    return this.http.put<City>(`${this.baseUrl}`,
      city,
      this.httpOptionsService.getHttpOptions()
    );
  }

}
