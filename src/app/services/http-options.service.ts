import {Injectable} from '@angular/core';
import {HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class HttpOptionsService {

  constructor() { }

  getHttpOptions(): {headers: HttpHeaders} {
    let jwt = localStorage.getItem('jwt');
    let httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json'
        }
      )
    };
    if (jwt != null) {
      httpOptions.headers = httpOptions.headers.append('Authorization', `Bearer ${jwt}`)
    }
    return httpOptions;
  }

  getHttpHeaders(): HttpHeaders {
    let jwt = localStorage.getItem('jwt');
    let headers= new HttpHeaders(
      {
        'Content-Type': 'application/json'
      }
    );
    if (jwt != null) {
      headers = headers.append('Authorization', `Bearer ${jwt}`)
    }
    return headers;
  }

}
