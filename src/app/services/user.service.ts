import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Reservation} from "../domain/reservation";
import {User} from "../domain/user";
import {HttpOptionsService} from "./http-options.service";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/user`;

  constructor() {
  }

  selectUsers(parameters?: {hotelId?: number, email?: string}) {
    let params = new HttpParams();
    if (parameters) {
      if (parameters["hotelId"]) {
        params = params.append("hotelId", parameters["hotelId"]);
      }
      if (parameters["email"]) {
        params = params.append("email", parameters["email"]);
      }
    }
    return this.http.get<User[]>(`${this.baseUrl}`, {
      headers: this.httpOptionsService.getHttpHeaders(),
      params: params
    });
  }

  sessionUserIsAdmin(): boolean {
    if (localStorage.getItem("user") == null) {
      return false;
    }
    return (JSON.parse(localStorage.getItem("user")!) as User)
      .roles.some(value => value.name === "ADMIN");
  }

  sessionUserIsLoggedIn(): boolean {
    if (localStorage.getItem("user") && localStorage.getItem("jwt")) {
      return true;
    }
    return false;
  }

  sessionUserIsMaintainer() {
    if (localStorage.getItem("user") == null) {
      return false;
    }
    return (JSON.parse(localStorage.getItem("user")!) as User)
      .roles.some(value => value.name === "MAINTAINER");
  }

  getUserRolesInString(user: User): string {
    let roles: string = "";
    for (let role of user.roles) {
      roles = roles.concat(role.name, " ");
    }
    return roles;
  }

  userIsAdmin(user: User) {
    return user.roles.some(value => value.name === "ADMIN");
  }

}
