import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthResponse} from "../domain/auth/auth-response";
import {AuthEmailRequest} from "../domain/auth/auth-email-request";
import {AuthPhoneRequest} from "../domain/auth/auth-phone-request";
import {SignUpRequest} from "../domain/auth/sign-up-request";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  http = inject(HttpClient);

  baseUrl = `${environment.apiUrl}/auth`;

  constructor() { }

  signUp(signUpRequest: SignUpRequest):Observable<AuthResponse> {
    return this.http.post<AuthResponse>(`${this.baseUrl}/signup`, signUpRequest);
  }

  signInByEmail(authReq: AuthEmailRequest) {
    return this.http.post<AuthResponse>(`${this.baseUrl}/email`, authReq);
  }

  signInByPhone(authReq: AuthPhoneRequest) {
    return this.http.post<AuthResponse>(`${this.baseUrl}/phone`, authReq);
  }

}
