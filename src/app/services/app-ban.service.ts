import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {HttpOptionsService} from "./http-options.service";
import {AppBanRequestDto} from "../domain/ban/app-ban-request-dto";
import {environment} from "../../environments/environment";
import {AppBan} from "../domain/ban/app-ban";

@Injectable({
  providedIn: 'root'
})
export class AppBanService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/app-ban`;

  constructor() { }

  selectAppBans() {
    return this.http.get<AppBan[]>(`${this.baseUrl}`, this.httpOptionsService.getHttpOptions());
  }

  insert(appBanRequestDto: AppBanRequestDto) {
    return this.http.post<AppBan>(`${this.baseUrl}`, appBanRequestDto, this.httpOptionsService.getHttpOptions());
  }

  delete(userId: number) {
    return this.http.delete<AppBan>(`${this.baseUrl}?userId=${userId}`, this.httpOptionsService.getHttpOptions());
  }

}
