import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Reservation} from "../domain/reservation";
import {HttpOptionsService} from "./http-options.service";
import {InsertReservationRequestDto} from "../domain/insert-reservation-request-dto";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/reservation`;

  constructor() {
  }

  insertReservation(insertReservationRequestDto: InsertReservationRequestDto) {
    return this.http.post<Reservation>(`${this.baseUrl}`,
      insertReservationRequestDto, this.httpOptionsService.getHttpOptions());
  }

  accept(reservationId: number) {
    return this.http.put<Reservation>(`${this.baseUrl}/${reservationId}/accept`,
      {
        accept: true
      }, this.httpOptionsService.getHttpOptions());
  }

  pay(reservationId: number) {
    return this.http.put<Reservation>(`${this.baseUrl}/${reservationId}/pay`,
      {
        pay: true
      }, this.httpOptionsService.getHttpOptions());
  }

  reject(reservationId: number) {
    return this.http.put<Reservation>(`${this.baseUrl}/${reservationId}/reject`,
      {
        reject: true
      }, this.httpOptionsService.getHttpOptions());
  }

  selectUserWaitingAcceptReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/user/waiting-accept`, this.httpOptionsService.getHttpOptions());
  }

  selectUserWaitingPaymentReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/user/waiting-payment`, this.httpOptionsService.getHttpOptions());
  }

  selectUserPaidReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/user/paid`, this.httpOptionsService.getHttpOptions());
  }

  selectHotelWaitingAcceptReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/hotel/waiting-accept`, this.httpOptionsService.getHttpOptions());
  }

  selectHotelWaitingPaymentReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/hotel/waiting-payment`, this.httpOptionsService.getHttpOptions());
  }

  selectHotelPaidReservations() {
    return this.http.get<Reservation[]>(`${this.baseUrl}/hotel/paid`, this.httpOptionsService.getHttpOptions());
  }

}
