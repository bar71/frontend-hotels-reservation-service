import {inject, Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {HttpOptionsService} from "./http-options.service";
import {Room} from "../domain/room";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  http = inject(HttpClient);

  httpOptionsService = inject(HttpOptionsService);

  baseUrl = `${environment.apiUrl}/room`;

  constructor() {
  }

  selectRooms(parameters?: { id?: number | undefined, roomClassId?: number | undefined,
                             hotelId?: number | undefined, placesCount?: number | undefined,}) {
    let params = new HttpParams();
    if (parameters) {
      if (parameters["id"]) {
        params = params.append("id", parameters["id"]);
      }
      if (parameters["roomClassId"]) {
        params = params.append("roomClassId", parameters["roomClassId"]);
      }
      if (parameters["hotelId"]) {
        params = params.append("hotelId", parameters["hotelId"]);
      }
      if (parameters["placesCount"]) {
        params = params.append("placesCount", parameters["placesCount"]);
      }
    }
    return this.http.get<Room[]>(`${this.baseUrl}`, {
      headers: this.httpOptionsService.getHttpHeaders(),
      params: params
      },
    );
  }

  selectRoomById(roomId: number) {
    return this.http.get<Room>(`${this.baseUrl}/${roomId}`, this.httpOptionsService.getHttpOptions());
  }

  selectFilteredRooms(parameters: {hotelId: number, startDate: string, finalDate: string, roomClassId: number, placesCount: number}) {
    let params = new HttpParams().appendAll(parameters);
    return this.http.get<Room[]>(`${this.baseUrl}/filtered`, {
        headers: this.httpOptionsService.getHttpHeaders(),
        params: params
      },
    );
  }

  insertRoom(room: Room) {
    return this.http.post<Room>(`${this.baseUrl}`,
      room,
      this.httpOptionsService.getHttpOptions());
  }

  updateRoom(room: Room) {
    return this.http.put<Room>(`${this.baseUrl}`,
      room,
      this.httpOptionsService.getHttpOptions());
  }

  deleteRoom(id: number) {
    return this.http.delete<Room>(`${this.baseUrl}/${id}`,
      this.httpOptionsService.getHttpOptions());
  }

}
