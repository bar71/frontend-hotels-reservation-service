import {User} from "./user";
import {Hotel} from "./hotel";
import {Room} from "./room";
import {Payment} from "./payment";

export interface Reservation {

  id: number;

  user: User;

  hotel: Hotel;

  room: Room;

  startDate: Date;

  finalDate: Date;

  isPaid: boolean;

  isAccepted: boolean;

  totalPrice: number;

  payment: Payment;
}
