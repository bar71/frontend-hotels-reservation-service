export interface Payment {

  id: number;

  reservationId: number;

  checkPrice: number;

  paymentDate: string;

}
