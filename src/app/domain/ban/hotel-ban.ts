import {User} from "../user";
import {Hotel} from "../hotel";

export interface HotelBan {
  id: number;
  user: User;
  judge: User;
  hotelId: number;
  reason: string;
  banDate: string;
}
