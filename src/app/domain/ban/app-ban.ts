import {User} from "../user";
import {Hotel} from "../hotel";

export interface AppBan {
  id: number;
  user: User;
  judge: User;
  reason: string;
  banDate: string;
}
