export interface HotelBanRequestDto {
  userId: number;
  reason: string;
}
