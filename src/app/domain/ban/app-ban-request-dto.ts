export interface AppBanRequestDto {
  email: string;
  reason: string;
}
