export interface InsertReservationRequestDto {

  hotelId: number;

  roomId: number;

  startDate: string;

  finalDate: string;

}
