export interface RoomClass {
  id: number;
  name: string;
}
