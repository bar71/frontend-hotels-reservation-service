import {Role} from "./role";

export interface User {
  id: number;
  name: string;
  phone: string;
  email: string;
  rawPassword: string;
  roles: Role[];
}
