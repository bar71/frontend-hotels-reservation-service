import {RoomClass} from "./room-class";

export interface Room {
  id: number;
  roomClass: RoomClass;
  hotelId: number;
  costPerDay: number;
  description: string;
  placesCount: number;
}
