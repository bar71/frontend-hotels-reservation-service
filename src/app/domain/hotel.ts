import {City} from "./city";

export interface Hotel {
  id: number;
  name: string;
  image: string;
  city: City;
  stars: number;
  address: string;
  description: string;
  distanceToCenter: number;
}
