export interface AuthEmailRequest {
  email: string;
  rawPassword: string;
}
