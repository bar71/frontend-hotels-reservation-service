export interface SignUpRequest {
  name: string;
  phone: string;
  email: string;
  rawPassword: string;
}
