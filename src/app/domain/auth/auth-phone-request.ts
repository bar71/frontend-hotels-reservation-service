export interface AuthPhoneRequest {
  phone: string;
  rawPassword: string;
}
